package io.javabrains.springsecurityjwt.repositories;

import io.javabrains.springsecurityjwt.models.entities.AuctionBidEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;


public interface AuctionBidsRepository extends JpaRepository<AuctionBidEntity, Long> {

    @Transactional
    @Modifying
    @Query("DELETE FROM AuctionBidEntity a WHERE a.auctionItem.id=:itemId")
    void deleteAllBidsOfTheItem(Long itemId);
}
