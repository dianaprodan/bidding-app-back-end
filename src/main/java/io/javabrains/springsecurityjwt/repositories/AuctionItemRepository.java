package io.javabrains.springsecurityjwt.repositories;

import io.javabrains.springsecurityjwt.models.entities.AuctionItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface AuctionItemRepository extends JpaRepository<AuctionItemEntity, Long> {

    @Transactional
    @Modifying
    @Query("DELETE FROM AuctionItemEntity a WHERE a.id IN :itemIds AND a.owner.username = :username")
    int deleteItemsOfTheUser(String username, List<Long> itemIds);
}
