package io.javabrains.springsecurityjwt.repositories;

import io.javabrains.springsecurityjwt.models.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> { }
