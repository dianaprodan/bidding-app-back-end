package io.javabrains.springsecurityjwt.services;

import io.javabrains.springsecurityjwt.exception.ResourceNotFoundException;
import io.javabrains.springsecurityjwt.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findById(username)
                .map(userEntity -> new User(userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>()))
                .orElseThrow(() -> new ResourceNotFoundException("user", "username", username));
    }
}