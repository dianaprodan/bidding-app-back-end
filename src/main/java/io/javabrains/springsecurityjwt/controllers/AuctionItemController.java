package io.javabrains.springsecurityjwt.controllers;

import io.javabrains.springsecurityjwt.exception.ResourceNotFoundException;
import io.javabrains.springsecurityjwt.models.Coordinate;
import io.javabrains.springsecurityjwt.models.dto.out.AuctionBidDTO;
import io.javabrains.springsecurityjwt.models.dto.out.AuctionItemDTO;
import io.javabrains.springsecurityjwt.models.dto.out.UserDTO;
import io.javabrains.springsecurityjwt.models.entities.AuctionBidEntity;
import io.javabrains.springsecurityjwt.models.entities.AuctionItemEntity;
import io.javabrains.springsecurityjwt.models.entities.AuctionItemStatus;
import io.javabrains.springsecurityjwt.models.entities.UserEntity;
import io.javabrains.springsecurityjwt.models.network.AuctionItemsResponse;
import io.javabrains.springsecurityjwt.models.network.ResourceIdResponse;
import io.javabrains.springsecurityjwt.repositories.AuctionBidsRepository;
import io.javabrains.springsecurityjwt.repositories.AuctionItemRepository;
import io.javabrains.springsecurityjwt.repositories.UserRepository;
import io.javabrains.springsecurityjwt.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/rest/latest/")
public class AuctionItemController {
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuctionItemRepository auctionItemRepository;

    @Autowired
    private AuctionBidsRepository auctionBidsRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/auctionItems", method = RequestMethod.GET)
    public ResponseEntity<?> getAllItems(@RequestHeader("Authorization") String authorization,
                                         @RequestParam("owners") String owners) {
        String username = jwtUtil.extractUsername(jwtUtil.extractTokenFromAuthorizationHeader(authorization));
        List<AuctionItemDTO> items = auctionItemRepository
                .findAll()
                .stream()
                .filter(auctionItemEntity -> {
                    if (owners.equals("@me")) {
                        return auctionItemEntity.owner.username.equals(username);
                    } else if (owners.equals("@others")) {
                        return !auctionItemEntity.owner.username.equals(username) && auctionItemEntity.currentStatus == AuctionItemStatus.Status.IN_AUCTION;
                    } else {
                        throw new ResourceNotFoundException("auction item", "owner", owners);
                    }
                })
                .map(auctionItemEntity -> {
                    final UserEntity ownerEntity = auctionItemEntity.owner;
                    final UserDTO ownerDTO = new UserDTO(ownerEntity.username,
                            ownerEntity.name, ownerEntity.address, ownerEntity.email,
                            ownerEntity.mobileNumber);
                    List<AuctionBidDTO> auctionBidDTOS = auctionBidsRepository
                            .findAll()
                            .stream()
                            .filter(auctionBidEntity ->
                                    auctionBidEntity.auctionItem.id.equals(auctionItemEntity.id))
                            .map(auctionBidEntity ->
                                    new AuctionBidDTO(auctionBidEntity.id,
                                    auctionBidEntity.bidder.email, auctionItemEntity.id, auctionBidEntity.amount,
                                    auctionBidEntity.createdAt))
                            .collect(Collectors.toList());
                    return new AuctionItemDTO(auctionItemEntity.id, auctionItemEntity.name, ownerDTO,
                            auctionItemEntity.description, auctionItemEntity.rating,
                            auctionItemEntity.currency, auctionItemEntity.startingPrice, auctionItemEntity.image,
                            new AuctionItemStatus(auctionItemEntity.currentStatus, auctionItemEntity.statusTimestamp),
                            new Coordinate(auctionItemEntity.latitude, auctionItemEntity.longitude), auctionBidDTOS);
                })
                .collect(Collectors.toList());
        return ResponseEntity.ok(new AuctionItemsResponse(items));
    }

    @RequestMapping(value = "/auctionItems", method = RequestMethod.POST)
    public ResponseEntity<?> createItem(@RequestBody io.javabrains.springsecurityjwt.models.dto.in.AuctionItemDTO auctionItemDTO,
                                        @RequestHeader("Authorization") String authorization) {
        String username = jwtUtil.extractUsername(jwtUtil.extractTokenFromAuthorizationHeader(authorization));
        final boolean isPlacedInAuction = false;
        final AuctionItemEntity auctionItemEntity = userRepository
                .findById(username)
                .map(userEntity -> new AuctionItemEntity(auctionItemDTO.name, userEntity, auctionItemDTO.description,
                        auctionItemDTO.rating, auctionItemDTO.currency, auctionItemDTO.startingPrice,
                        auctionItemDTO.image.getBytes(), new AuctionItemStatus(), auctionItemDTO.coordinate))
                .orElseThrow(() -> new ResourceNotFoundException("user", "username", username));
        final AuctionItemEntity storedAuctionItemEntity = auctionItemRepository.saveAndFlush(auctionItemEntity);
        return ResponseEntity.ok(new ResourceIdResponse<>(storedAuctionItemEntity.id));
    }

    @RequestMapping(value = "/auctionItems", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteItem(@RequestParam("auctionItemId") Long auctionItemId,
                                        @RequestHeader("Authorization") String authorization) {
        String username = jwtUtil.extractUsername(jwtUtil.extractTokenFromAuthorizationHeader(authorization));
        return auctionItemRepository
                .findById(auctionItemId)
                .map(auctionItemEntity -> {
                    if (!auctionItemEntity.owner.username.equals(username)) {
                        throw new RuntimeException("This item does not belong to you");
                    }
                    auctionBidsRepository.deleteAllBidsOfTheItem(auctionItemId);
                    auctionItemRepository.deleteById(auctionItemId);
                    return ResponseEntity.ok().build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("auction item", "id", auctionItemId));
    }

    @RequestMapping(value = "auctionItems/status/change", method = RequestMethod.PUT)
    public ResponseEntity<?> placeInAuction(@RequestHeader("Authorization") String authorization,
                                            @RequestParam("auctionItemId") Long auctionItemId,
                                            @RequestBody AuctionItemStatus currentStatus) {
        String username = jwtUtil.extractUsername(jwtUtil.extractTokenFromAuthorizationHeader(authorization));
        return auctionItemRepository
                .findById(auctionItemId)
                .map(auctionItemEntity -> {
                    if (!auctionItemEntity.owner.username.equals(username)) {
                        throw new RuntimeException("This item does not belong to you");
                    }

                    auctionItemEntity.currentStatus = currentStatus.status;
                    auctionItemEntity.statusTimestamp = currentStatus.timestamp;
                    auctionItemRepository.saveAndFlush(auctionItemEntity);
                    return ResponseEntity.ok().build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("auction item", "id", auctionItemId));
    }

    @RequestMapping(value = "/auctionItems/bids/add", method = RequestMethod.PUT)
    public ResponseEntity<?> addBid(@RequestParam("auctionItemId") Long auctionItemId, @RequestParam("amount") Double amount,
                                    @RequestHeader("Authorization") String authorization) {
        String username = jwtUtil.extractUsername(jwtUtil.extractTokenFromAuthorizationHeader(authorization));
        final AuctionBidEntity auctionBidEntity = new AuctionBidEntity(new UserEntity(username),
                new AuctionItemEntity(auctionItemId), amount);
        return ResponseEntity.ok(new ResourceIdResponse<>(auctionBidsRepository.saveAndFlush(auctionBidEntity).id));
    }

    @RequestMapping(value = "/auctionItems/rating/add", method = RequestMethod.PUT)
    public ResponseEntity<?> addRating(@RequestParam("auctionItemId") Long auctionItemId, @RequestParam("rating") Double rating) {
        return auctionItemRepository
                .findById(auctionItemId)
                .map(auctionItemEntity -> {
                    auctionItemEntity.rating = auctionItemEntity.rating + ',' + rating;
                    auctionItemRepository.saveAndFlush(auctionItemEntity);
                    return ResponseEntity.ok().build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("auction item", "id", auctionItemId));
    }
}
