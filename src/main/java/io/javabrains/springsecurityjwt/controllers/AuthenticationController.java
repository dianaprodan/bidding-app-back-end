package io.javabrains.springsecurityjwt.controllers;

import io.javabrains.springsecurityjwt.exception.BadRequestException;
import io.javabrains.springsecurityjwt.exception.ResourceNotFoundException;
import io.javabrains.springsecurityjwt.models.dto.in.UserDTO;
import io.javabrains.springsecurityjwt.models.dto.out.CheckTokenResponse;
import io.javabrains.springsecurityjwt.models.entities.UserEntity;
import io.javabrains.springsecurityjwt.models.network.*;
import io.javabrains.springsecurityjwt.repositories.UserRepository;
import io.javabrains.springsecurityjwt.services.UserService;
import io.javabrains.springsecurityjwt.util.JwtUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/rest/latest/")
class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@NotNull @RequestBody AuthenticationRequest authenticationRequest) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        final UserEntity userEntity = userRepository
                .findById(authenticationRequest.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("user", "username", authenticationRequest.getUsername()));
        final UserDetails userDetails = new User(userEntity.getUsername(), userEntity.getPassword(), new ArrayList<>());
        final String jwt = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt, new io.javabrains.springsecurityjwt.models.dto.out.UserDTO(
            userEntity.username, userEntity.name, userEntity.address, userEntity.email, userEntity.mobileNumber
        )));
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> registerUser(@NotNull @RequestBody UserDTO userDTO) {
        userRepository
                .findById(userDTO.username)
                .ifPresent(userEntity -> {
                    throw new BadRequestException("User with this username already exists");
                });
        final UserEntity userEntity = new UserEntity(userDTO.username, userDTO.password,
                userDTO.name, userDTO.address, userDTO.email, userDTO.mobileNumber);
        userRepository.saveAndFlush(userEntity);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/checkToken", method = RequestMethod.POST)
    public ResponseEntity<?> checkToken(@NotNull @RequestBody CheckTokenRequest checkTokenRequest) {
        return jwtTokenUtil.isTokenExpired(checkTokenRequest.token)
                ? ResponseEntity.status(HttpStatus.GONE).build()
                : ResponseEntity.ok(new CheckTokenResponse(true, new JwtUtil().extractUsername(checkTokenRequest.token)));
    }
}
