package io.javabrains.springsecurityjwt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
    private String causedBy;

    public BadRequestException(String causedBy) {
        super(String.format("Bad Request caused by : '%s'", causedBy));
        this.causedBy = causedBy;
    }

    public String getCausedBy() {
        return this.causedBy;
    }
}
