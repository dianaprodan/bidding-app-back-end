package io.javabrains.springsecurityjwt.models.network;

import org.jetbrains.annotations.Contract;

public class ResourceIdResponse <T> {
    public T id;

    @Contract(pure = true)
    public ResourceIdResponse() {}

    public ResourceIdResponse(T id) {
        this.id = id;
    }
}
