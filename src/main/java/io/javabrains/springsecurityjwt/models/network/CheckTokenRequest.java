package io.javabrains.springsecurityjwt.models.network;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;

public class CheckTokenRequest implements Serializable {
    public String token;

    @Contract(pure = true)
    public CheckTokenRequest() {}

    @Contract(pure = true)
    public CheckTokenRequest(String token) {
        this.token = token;
    }
}
