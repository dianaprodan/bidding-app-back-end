package io.javabrains.springsecurityjwt.models.network;

import io.javabrains.springsecurityjwt.models.dto.out.AuctionItemDTO;

import java.util.List;

public class AuctionItemsResponse {
    public List<AuctionItemDTO> items;

    public AuctionItemsResponse() {
    }

    public AuctionItemsResponse(List<AuctionItemDTO> items) {
        this.items = items;
    }
}
