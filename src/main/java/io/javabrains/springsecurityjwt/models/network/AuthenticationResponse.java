package io.javabrains.springsecurityjwt.models.network;

import io.javabrains.springsecurityjwt.models.dto.out.UserDTO;
import org.jetbrains.annotations.Contract;

import java.io.Serializable;

public class AuthenticationResponse implements Serializable {
    private final String jwt;
    private final UserDTO user;

    @Contract(pure = true)
    public AuthenticationResponse(String jwt, UserDTO userDTO) {
        this.jwt = jwt;
        this.user = userDTO;
    }

    public String getJwt() {
        return jwt;
    }

    public UserDTO getUser() { return user; }
}
