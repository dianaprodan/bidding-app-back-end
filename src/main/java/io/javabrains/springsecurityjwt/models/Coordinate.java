package io.javabrains.springsecurityjwt.models;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;

public class Coordinate implements Serializable {
    public Double latitude;
    public Double longitude;

    @Contract(pure = true)
    public Coordinate() {}

    @Contract(pure = true)
    public Coordinate(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
