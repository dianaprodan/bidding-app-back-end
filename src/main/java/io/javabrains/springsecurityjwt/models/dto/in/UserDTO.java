package io.javabrains.springsecurityjwt.models.dto.in;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;

public class UserDTO implements Serializable {
    public String username;
    public String password;
    public String name;
    public String address;
    public String email;
    public String mobileNumber;

    //need default constructor for JSON Parsing
    @Contract(pure = true)
    public UserDTO() {}

    @Contract(pure = true)
    public UserDTO(String username, String password, String name, String address, String email, String mobileNumber) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.address = address;
        this.email = email;
        this.mobileNumber = mobileNumber;
    }
}
