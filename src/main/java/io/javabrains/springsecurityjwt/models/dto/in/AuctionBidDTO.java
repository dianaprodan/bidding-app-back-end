package io.javabrains.springsecurityjwt.models.dto.in;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;

public class AuctionBidDTO implements Serializable {
    public Long auctionItemId;
    public Double amount;

    @Contract(pure = true)
    public AuctionBidDTO() { }

    @Contract(pure = true)
    public AuctionBidDTO(Long auctionItemId, Double amount) {
        this.auctionItemId = auctionItemId;
        this.amount = amount;
    }
}
