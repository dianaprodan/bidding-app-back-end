package io.javabrains.springsecurityjwt.models.dto.out;

import io.javabrains.springsecurityjwt.models.Coordinate;
import io.javabrains.springsecurityjwt.models.entities.AuctionItemStatus;
import io.javabrains.springsecurityjwt.models.entities.Currency;
import org.jetbrains.annotations.Contract;

import java.io.Serializable;
import java.util.List;

public class AuctionItemDTO implements Serializable {
    public Long id;
    public String name;
    public UserDTO owner;
    public String description;
    public String rating;
    public Currency currency;
    public Double startingPrice;
    public String image;
    public AuctionItemStatus currentStatus;
    public Coordinate coordinate;
    public List<AuctionBidDTO> bids;

    @Contract(pure = true)
    public AuctionItemDTO() { }

    @Contract(pure = true)
    public AuctionItemDTO(Long id, String name, UserDTO owner, String description, String rating, Currency currency,
                          Double startingPrice, byte[] image, AuctionItemStatus currentStatus, Coordinate coordinate,
                          List<AuctionBidDTO> bids) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.rating = rating;
        this.currency = currency;
        this.startingPrice = startingPrice;
        this.image = new String(image);
        this.currentStatus = currentStatus;
        this.coordinate = coordinate;
        this.bids = bids;
    }
}
