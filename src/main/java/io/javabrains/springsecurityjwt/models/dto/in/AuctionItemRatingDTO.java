package io.javabrains.springsecurityjwt.models.dto.in;

import java.io.Serializable;

public class AuctionItemRatingDTO implements Serializable {
    public Long auctionItemId;
    public Double rating;

    public AuctionItemRatingDTO() {
    }

    public AuctionItemRatingDTO(Long auctionItemId, Double rating) {
        this.auctionItemId = auctionItemId;
        this.rating = rating;
    }
}
