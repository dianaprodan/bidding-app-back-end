package io.javabrains.springsecurityjwt.models.dto.out;

import org.jetbrains.annotations.Contract;
import java.io.Serializable;

public class UserDTO implements Serializable {
    public String username;
    public String name;
    public String address;
    public String email;
    public String mobileNumber;

    @Contract(pure = true)
    public UserDTO() {}

    @Contract(pure = true)
    public UserDTO(String username, String name, String address, String email, String mobileNumber) {
        this.username = username;
        this.name = name;
        this.address = address;
        this.email = email;
        this.mobileNumber = mobileNumber;
    }
}
