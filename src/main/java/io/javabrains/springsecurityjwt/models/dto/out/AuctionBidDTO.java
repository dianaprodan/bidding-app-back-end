package io.javabrains.springsecurityjwt.models.dto.out;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;
import java.util.Date;

public class AuctionBidDTO implements Serializable {
    public Long id;
    public String bidderId;
    public Long auctionItemId;
    public Double amount;
    public Date createdAt;

    @Contract(pure = true)
    public AuctionBidDTO() { }

    @Contract(pure = true)
    public AuctionBidDTO(Long id, String bidderId, Long auctionItemId, Double amount, Date createdAt) {
        this.id = id;
        this.bidderId = bidderId;
        this.auctionItemId = auctionItemId;
        this.amount = amount;
        this.createdAt = createdAt;
    }
}
