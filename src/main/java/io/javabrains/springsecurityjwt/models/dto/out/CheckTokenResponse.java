package io.javabrains.springsecurityjwt.models.dto.out;

import java.io.Serializable;

public class CheckTokenResponse implements Serializable {
    public Boolean valid;
    public String userId;

    public CheckTokenResponse(Boolean valid, String userId) {
        this.valid = valid;
        this.userId = userId;
    }
}
