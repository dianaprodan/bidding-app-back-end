package io.javabrains.springsecurityjwt.models.dto.in;

import io.javabrains.springsecurityjwt.models.Coordinate;
import io.javabrains.springsecurityjwt.models.entities.Currency;
import org.jetbrains.annotations.Contract;

public class AuctionItemDTO {
    public String name;
    public String description;
    public String rating;
    public Currency currency;
    public Double startingPrice;
    public String image;
    public Coordinate coordinate;

    @Contract(pure = true)
    public AuctionItemDTO() { }

    @Contract(pure = true)
    public AuctionItemDTO(String name, String description, String rating, Currency currency, Double startingPrice, String image, Coordinate coordinate) {
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.currency = currency;
        this.startingPrice = startingPrice;
        this.image = image;
        this.coordinate = coordinate;
    }
}
