package io.javabrains.springsecurityjwt.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.Contract;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class UserEntity implements Serializable {
    @Id
    @NotBlank
    public String username;

    @NotBlank
    public String password;

    @NotBlank
    public String name;

    @NotBlank
    public String address;

    @NotBlank
    public String email;

    @NotBlank
    public String mobileNumber;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    public Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    public Date updatedAt;

    @Contract(pure = true)
    public UserEntity() {}

    @Contract(pure = true)
    public UserEntity(@NotBlank String username) {
        this.username = username;
    }

    @Contract(pure = true)
    public UserEntity(String username, String password, String name, String address, String email, String mobileNumber) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.address = address;
        this.email = email;
        this.mobileNumber = mobileNumber;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
