package io.javabrains.springsecurityjwt.models.entities;

public enum Currency {
    EUR, USD
}
