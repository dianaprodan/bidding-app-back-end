package io.javabrains.springsecurityjwt.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "auction_bids")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(allowGetters = true)
@Data
@NoArgsConstructor
public class AuctionBidEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @ManyToOne
    @JoinColumn(name = "bidderId", referencedColumnName = "username")
    public UserEntity bidder;

    @ManyToOne
    @JoinColumn(name = "auctionItemId", referencedColumnName = "id")
    public AuctionItemEntity auctionItem;

    public Double amount;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    public Date createdAt;
    
    public AuctionBidEntity() {}

    public AuctionBidEntity(UserEntity bidder, AuctionItemEntity auctionItem, Double amount) {
        this.bidder = bidder;
        this.auctionItem = auctionItem;
        this.amount = amount;
        this.createdAt = new Date();
    }
}
