package io.javabrains.springsecurityjwt.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.javabrains.springsecurityjwt.models.Coordinate;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Contract;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "auction_items")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
@NoArgsConstructor
@Getter @Setter
public class AuctionItemEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @NotBlank
    public String name;

    @ManyToOne
    @JoinColumn(name = "ownerId", referencedColumnName = "username")
    public UserEntity owner;

    @NotBlank
    public String description;

    public String rating;

    @Enumerated(EnumType.STRING)
    public Currency currency;

    @Column(nullable = false)
    public Double startingPrice;

    @Lob
    @Column(length = 307200)
    public byte[] image;

    @Enumerated(EnumType.STRING)
    public AuctionItemStatus.Status currentStatus;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date statusTimestamp;

    @Column(nullable = false)
    public Double latitude;

    @Column(nullable = false)
    public Double longitude;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    public Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    public Date updatedAt;
    
    public AuctionItemEntity() {}

    @Contract(pure = true)
    public AuctionItemEntity(Long id) {
        this.id = id;
    }

    @Contract(pure = true)
    public AuctionItemEntity(String name, UserEntity owner, String description, String rating, Currency currency,
                             Double startingPrice, byte[] image, AuctionItemStatus.Status currentStatus, Date statusTimestamp,
                             Coordinate coordinate) {
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.rating = rating;
        this.currency = currency;
        this.startingPrice = startingPrice;
        this.image = image;
        this.currentStatus = currentStatus;
        this.statusTimestamp = statusTimestamp;
        this.latitude = coordinate.latitude;
        this.longitude = coordinate.longitude;
    }

    @Contract(pure = true)
    public AuctionItemEntity(String name, UserEntity owner, String description, String rating, Currency currency,
                             Double startingPrice, byte[] image, AuctionItemStatus status, Coordinate coordinate) {
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.rating = rating;
        this.currency = currency;
        this.startingPrice = startingPrice;
        this.image = image;
        this.currentStatus = status.status;
        this.statusTimestamp = status.timestamp;
        this.latitude = coordinate.latitude;
        this.longitude = coordinate.longitude;
    }
}
