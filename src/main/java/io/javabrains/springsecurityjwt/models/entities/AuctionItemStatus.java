package io.javabrains.springsecurityjwt.models.entities;

import java.io.Serializable;
import java.util.Date;

public class AuctionItemStatus implements Serializable {
    public final Status status;
    public final Date timestamp;

    public AuctionItemStatus() {
        this.status = Status.CREATED;
        this.timestamp = new Date();
    }

    private AuctionItemStatus(Status status) {
        this.status = status;
        this.timestamp = new Date();
    }

    public AuctionItemStatus(Status status, Date timestamp) {
        this.status = status;
        this.timestamp = timestamp;
    }

    public static AuctionItemStatus nextStateFrom(AuctionItemStatus currentStatus) {
        return new AuctionItemStatus(currentStatus.status.nextStatus());
    }

    public enum Status {
        CREATED{
            @Override
            public Status nextStatus() {
                return Status.IN_AUCTION;
            }
        },
        IN_AUCTION{
            @Override
            public Status nextStatus() {
                return Status.SOLD_OUT;
            }
        },
        SOLD_OUT{
            @Override
            public Status nextStatus() {
                throw new RuntimeException("Final state");
            }
        };

        public abstract Status nextStatus();
    }
}
